package com.fortytwo.baggagesystem.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.fortytwo.baggagesystem.R;
import com.fortytwo.baggagesystem.adapter.FlightAdapter;

import com.fortytwo.baggagesystem.adapter.RecycleViewAdapter;
import com.fortytwo.baggagesystem.model.Baggages;
import com.fortytwo.baggagesystem.model.Flight;
import com.fortytwo.baggagesystem.model.Passenger;
import com.fortytwo.baggagesystem.service.BaggageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    final Context context = this;

    ListView listView;

    RecyclerView recyclerView;

    RecycleViewAdapter recycleViewAdapter;

    BaggageService baggageService;

    FloatingActionButton floatingActionButton;

    Baggages baggages;

    //Passenger passenger;

    List<Baggages> baggageList;

    final String firebaseUrl = "https://rfid-proj-a2226.firebaseio.com";

    Retrofit retrofit;

    Gson gson;

    SQLiteDatabase sqLiteDatabase;

    SwipeRefreshLayout swipeRefresh;

    TextView passengerNameText;

    TextView flightNoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = (ImageView) findViewById(R.id.airplaneImg);

        passengerNameText =(TextView)findViewById(R.id.passengerName);
        flightNoText = (TextView)findViewById(R.id.flightNo);

        baggageList = new ArrayList<>();

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPrompt();
                System.out.println(" BAGGAGE LIST " + baggageList.size());

            }
        });

        gson = new GsonBuilder()
                .setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(firebaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        baggageService = retrofit.create(BaggageService.class);


        try {
            String passengerId = getRecentPassenger();
            if (passengerId != null) {
                swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            recycleViewAdapter.clear();
                            fetchPassenger(getRecentPassenger());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                fetchPassenger(passengerId);
            } else {
                openPrompt();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
       /* try {
            System.out.println("PASSENGER " +fetchPassenger());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    private void fetchPassenger(final String passengerNumber) throws IOException {

        Call<Passenger> call = baggageService.getPassenger(passengerNumber);

        call.enqueue(new Callback<Passenger>() {
            @Override
            public void onResponse(Call<Passenger> call, Response<Passenger> response) {
                Passenger passenger = response.body();
                try {
                    if (passenger != null) {
                        sqLiteDatabase.execSQL("INSERT INTO Passenger VALUES('" + passengerNumber + "');");
                        passengerNameText.setText(passenger.getFirstName() + " " + passenger.getLastName());
                        flightNoText.setText(passenger.getFlight().keySet().toArray(new String[0])[0]);
                        fetchBaggage(passenger);
                    }
                    else{
                        Toast.makeText(context, "Passenger ID not exist", Toast.LENGTH_LONG).show();
                        recycleViewAdapter.clear();
                        fetchPassenger(getRecentPassenger());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Passenger> call, Throwable t) {
                System.out.println(" - - - FAIL - - -" + t);
            }
        });

    }

    private void fetchBaggage(Passenger passenger) throws IOException {

        for (final Map.Entry<String, Object> entry : passenger.getBaggages().entrySet()) {

            System.out.print(" BAG NO " + entry.getKey());
            Call<Baggages> call = baggageService.getBaggages(entry.getKey());

            call.enqueue(new Callback<Baggages>() {

                @Override
                public void onResponse(Call<Baggages> call, Response<Baggages> response) {

                    baggages = response.body();
                    baggages.setBaggageNumber(entry.getKey());
                    //baggages.setDescription("Blue big bag with fur on the side");
                    baggageList.add(baggages);
                    System.out.println("BAGGAGE " + baggageList.get(0).getStatus());
                    recycleViewAdapter = new RecycleViewAdapter(baggageList);
                    //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(recycleViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    swipeRefresh.setRefreshing(false);
                }

                @Override
                public void onFailure(Call<Baggages> call, Throwable t) {

                    System.out.println(" - - - FAIL - - -" + t);

                }

            });
        }
    }

    private void openPrompt() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View promptView = layoutInflater.inflate(R.layout.add_baggage_prompt, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptView);
        final EditText userInput = (EditText) promptView.findViewById(R.id.passengerNumberInput);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                try {
                                    baggageList.clear();
                                    String userInputString = userInput.getText().toString();
                                    if (TextUtils.isEmpty(userInputString)) {
                                        return;
                                    } else {
                                        fetchPassenger(userInputString);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private String getRecentPassenger() {
        sqLiteDatabase = openOrCreateDatabase("passengerDb", MODE_PRIVATE, null);
        String recentId = null;
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Passenger;");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Passenger(Passenger VARCHAR);");
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Passenger", null);
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            cursor.moveToLast();
            recentId = cursor.getString(0);
        }
        return recentId;
    }

}

