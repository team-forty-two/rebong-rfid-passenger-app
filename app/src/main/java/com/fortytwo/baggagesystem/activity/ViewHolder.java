package com.fortytwo.baggagesystem.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fortytwo.baggagesystem.R;


/**
 * Created by APN User on 3/10/2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView baggageNumber;

    public TextView status;

    public TextView baggageDescription;



    public ViewHolder(View itemView) {
        super(itemView);
        baggageNumber = (TextView) itemView.findViewById(R.id.baggageNumber);
        status = (TextView) itemView.findViewById(R.id.statusText);
        baggageDescription = (TextView) itemView.findViewById(R.id.baggageDescription);
    }


}
