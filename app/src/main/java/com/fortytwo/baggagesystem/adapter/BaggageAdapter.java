package com.fortytwo.baggagesystem.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fortytwo.baggagesystem.model.Flight;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by APN User on 3/10/2018.
 */

public class BaggageAdapter extends BaseAdapter {

    private final ArrayList baggageList;

    public BaggageAdapter(Map<String, Object> baggageMap) {
        baggageList = new ArrayList();
        baggageList.addAll(baggageMap.entrySet());
    }

    @Override
    public int getCount() {
        return baggageList.size();
    }

    @Override
    public Object getItem(int position) {
        return (Map.Entry<String, Object>) baggageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
