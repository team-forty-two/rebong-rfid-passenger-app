package com.fortytwo.baggagesystem.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fortytwo.baggagesystem.R;
import com.fortytwo.baggagesystem.model.Flight;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by APN User on 3/9/2018.
 */

public class FlightAdapter extends BaseAdapter {
private final ArrayList mData;

    public FlightAdapter(Map<String, Flight> map){
    mData=new ArrayList();
    mData.addAll(map.entrySet());
    }


    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, Flight> getItem(int position) {
        return (Map.Entry<String, Flight>) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_list, parent, false);
        } else {
            result = convertView;
        }

        Map.Entry<String, Flight> item = getItem(position);

        // TODO replace findViewById by ViewHolder
        ((TextView) result.findViewById(R.id.departure)).setText(item.getValue().getDeparture());
        ((TextView) result.findViewById(R.id.destination)).setText(item.getValue().getDestination());
        ((TextView) result.findViewById(R.id.number)).setText(item.getValue().getNumber());

        return result;
    }
}
