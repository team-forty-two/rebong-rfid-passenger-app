package com.fortytwo.baggagesystem.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortytwo.baggagesystem.R;
import com.fortytwo.baggagesystem.activity.ViewHolder;
import com.fortytwo.baggagesystem.model.Baggages;
import com.fortytwo.baggagesystem.model.Flight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APN User on 3/10/2018.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Baggages> listBaggage;

    public RecycleViewAdapter(List<Baggages> baggages){
        listBaggage=baggages;
    }

    public void clear() {
        listBaggage.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Baggages> list) {
        listBaggage.addAll(list);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.baggage_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       Baggages baggages = listBaggage.get(position);
       String status = baggages.getStatus();
       holder.status.setText(status);
        switch ((status)){
            case "1":holder.status.setBackgroundResource(R.color.colorFirstStation); break;
            case "2":holder.status.setBackgroundResource(R.color.colorSecondStation); break;
            case "3":holder.status.setBackgroundResource(R.color.colorThirdStation); break;
            default: holder.status.setBackgroundResource(R.color.colorUnknownStation); break;
        }

       holder.baggageNumber.setText(baggages.getBaggageNumber());
       holder.baggageDescription.setText(baggages.getDescription());
    }

    @Override
    public int getItemCount() {
        return listBaggage.size();
    }
}
