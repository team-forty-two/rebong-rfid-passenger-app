package com.fortytwo.baggagesystem.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by APN User on 3/10/2018.
 */

public class Baggages {

    String baggageNumber;

    String status;

    String description;

    HashMap<String, Object> user;

    public String getBaggageNumber() {
        return baggageNumber;
    }

    public void setBaggageNumber(String baggageNumber) {
        this.baggageNumber = baggageNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HashMap<String, Object> getUser() {
        return user;
    }

    public void setUser(HashMap<String, Object> user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
