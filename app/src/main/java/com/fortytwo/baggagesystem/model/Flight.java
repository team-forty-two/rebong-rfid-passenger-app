package com.fortytwo.baggagesystem.model;

/**
 * Created by APN User on 3/7/2018.
 */

public class Flight {
    String departure;
    String destination;
    String number;

    public Flight(){

    }

    public Flight(String departure, String destination, String number) {
        this.departure = departure;
        this.destination = destination;
        this.number = number;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
