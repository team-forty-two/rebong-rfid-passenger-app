package com.fortytwo.baggagesystem.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by APN User on 3/10/2018.
 */

public class Passenger {

    HashMap<String, Object> baggages;

    String contactNumber;

    String firstName;

    HashMap<String, Object> flight;

    String lastName;

    public HashMap<String, Object> getBaggages() {
        return baggages;
    }

    public void setBaggages(HashMap<String, Object> baggages) {
        this.baggages = baggages;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public HashMap<String, Object> getFlight() {
        return flight;
    }

    public void setFlight(HashMap<String, Object> flight) {
        this.flight = flight;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "baggages=" + baggages +
                ", contactNumber='" + contactNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", flight=" + flight +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
