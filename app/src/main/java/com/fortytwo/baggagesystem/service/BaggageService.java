package com.fortytwo.baggagesystem.service;

import com.fortytwo.baggagesystem.model.Baggages;
import com.fortytwo.baggagesystem.model.Flight;
import com.fortytwo.baggagesystem.model.Passenger;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by APN User on 3/9/2018.
 */

public interface BaggageService {

    @GET("/flights.json")
    Call<Map<String,Flight>> getFlights();

    @GET("/passengers/{passengerId}.json")
    Call<Passenger> getPassenger(@Path("passengerId") String passengerId);


    @GET("/baggages/{baggageId}.json")
    Call<Baggages> getBaggages(@Path("baggageId") String baggage);


}
